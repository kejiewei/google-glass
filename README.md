# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Deployment instructions
* Database configuration

1. 数据库使用：mysql -u root -p，密码为yuxiao123

2. 选择数据库：mysql> use facedb;

3. 查询：mysql> select * from person; 

### How to run tests ###

* 进入server，执行python manage.py runserver 0.0.0.0:8080启动服务器
* 手机端可能使用到的API位于server/server/views.py下，按照调用顺序列举如下：

1. getPersonList，获取所有用户列表，返回id和用户名，用户名可用于下拉框的数据源
 
2. postAddPerson，在人脸库中添加一名用户，返回用户名，当从下拉框选择一用户名后需要发送该请求。

      如：curl -A "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)" -d "person_name=troy" 115.28.246.138:8080/postAddPerson/

      {"person_name": "troy"}

3. postAddFace，对某用户添加一张图片，返回用户名和图片路径，拍照后上传需要发送该请求，只处理一张图片。

      如：curl -A "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)" -F "file=@C:\Users\kjw\Desktop\2.jpg" -F "person_name=troy" 115.28.246.138:8080/postAddFace/

      {"person_name": "troy", "file": "/root/google-glass/server/server/albums/troy_20

14-06-07_2.jpg"}

4. postTrain，添加完图片后，执行训练，返回{"request":"train"}。

      如：curl -A "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)"  115.28.246.138:8080/postTrain/

      {"request": "train"}

5. postFaceToIdentify，拍摄照片，上传并进行识别，如果成功，返回人物信息；否则返回{"error":错误信息}。识别时调用。

      如：curl -A "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)" -F "file=@C:\Users\kjw\Desktop\2.jpg"  115.28.246.138:8080/postFaceToIdentify/

      {"qq": null, "s_high_school": null, "name": "troy", "mobile": null, "hometown": null, "sex": null, "renren": "123456", "primary_school": null, "college": null, "j_high_school": null, "birth_date": null}

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
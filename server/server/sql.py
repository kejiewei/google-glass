# -*- coding: utf-8 -*-     
#mysqldb    
import time, MySQLdb    
   
class MysqlDB():
	def __init__(self):
		self.dbName = "facedb"
		self.fields = "name, sex, birth_date, hometown, mobile, qq, renren, college, s_high_school, j_high_school, primary_school"
		self.fieldList = ["name", "sex", "birth_date", "hometown", "mobile", "qq", "renren", "college", "s_high_school", "j_high_school", "primary_school"]
		self.fmt = "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s"
	
		self.conn = MySQLdb.connect(host="localhost",user="root",passwd="yuxiao123",charset="utf8",unix_socket='/tmp/mysql.sock')  
		self.cursor =self.conn.cursor()
		#self.cursor.execute('create database if not exists ' + self.dbName)
		#self.conn.commit()
		
		self.conn.select_db(self.dbName)
		self.cursor =self.conn.cursor()
		sql = "create table if not exists person(id int(20) not null AUTO_INCREMENT, name varchar(128), sex varchar(4), birth_date varchar(32), hometown varchar(128), mobile varchar(32), qq varchar(16), renren varchar(16), college varchar(128), s_high_school varchar(128), j_high_school varchar(128), primary_school varchar(128), primary key (id))" 
		#self.cursor.execute(sql)

	def insert(self, infoDict):
		#写入
		tmpList = []
		if not infoDict.get("name"):
			print "请输入名字"
			return
		sql = "insert into person(" + self.fields + ") values(" + self.fmt + ")"
		print sql
		for field in self.fieldList:
			tmpList.append(infoDict.get(field))
		print tmpList
		param = tuple(tmpList)
		n = self.cursor.execute(sql,param)
		print n
		
	def update(self, name, infoDict):
		#更新
		tmpList = []
		sql = "update person set "
		for key, value in infoDict:
			if key in self.fieldList:
				sql = sql + str(key) + " = " + str(value) + ","
		sql = sql[:-1] + "where name = " + name
		n = self.cursor.execute(sql)
		print n

	def select(self, name):
		#查询    
		tmpDict = {}
		n = self.cursor.execute("select * from person where name = '" + name + "'")    
		row = self.cursor.fetchone() 
		for i in range(1,12):
			tmpDict[self.fieldList[i - 1]] = row[i]
		return tmpDict
	
	def selectAll(self):
		tmpDict = {}
		n = self.cursor.execute("select id, name from person")
		for row in self.cursor.fetchall():
			tmpDict[row[0]] = row[1]
		return tmpDict
				
	def delete(self, name):
		#删除
		sql = "delete from person where name= " + name
		n = self.cursor.execute(sql)    
		print n

	def close(self):
		#关闭
		self.cursor.close()
		self.conn.close()   


#db = MysqlDB()
#db.insert({"name":"troy", "sex":"男"})

 

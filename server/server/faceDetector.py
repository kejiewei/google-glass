#!/usr/bin/python
# -*- coding:utf-8 -*-

from facepp import API, File

import sys
import os
import os.path
import time

'''
	Function:		
	Description:	
	Calls:			
	Called By: 		
	Input:			
	Return: 		
'''
CONFIDENCE_LIMIT = 20

class FaceDetector():
	def __init__(self):
		#fdir = os.path.dirname(__file__)
		#f = open(os.path.join(fdir, 'apikey.cfg'))
		#exec(f.read())
		#srv = locals().get('SERVER')
		SERVER = 'http://apius.faceplusplus.com/'
		API_KEY = '6b6afec43a1cfa0498cdd1e57b8d3c3b'
		API_SECRET = 'TlcJ15UNb4DM7uHuBgiLaxI4rnRppH6C'
		self.api =  API(API_KEY, API_SECRET, srv = SERVER)

	'''
	Function:		addFaceToPerson
	Description:	向用户添加一张头像
	Input:			image 图片本地路径
					person_name 用户名
	Return: 		0 表示错误
					否则返回成功添加的图片数量，一般为1
	'''
	def addFaceToPerson(self, image, person_name):
		try:
			img_info = self.api.detection.detect(img = File(image))
		except:
			return 0
		#排除找不到脸或者找到多张脸的情况
		if len(img_info['face']) != 1:
			return 0
		face_id = img_info['face'][0]['face_id']
		#print face_id
		return self.api.person.add_face(person_name = person_name, face_id = face_id)['added']

	def delPerson(self, person_name):
		try:
			self.api.person.delete(person_name = person_name)
		except:
			print "删除失败"

	'''Function:		addPerson
	Description:	向人脸库添加一名新的用户
	Input:			image_list 图片本地路径列表
					person_name 用户名
	Return: 		无
	'''
	def addPerson(self, person_name, image_list = []):
		try:
			self.api.person.delete(person_name = person_name)
		except:
			print "删除失败"	
		try:
			self.api.person.create(person_name = person_name)
			print "添加person" + person_name  + "成功"
		except:
			pass
		
		#先检测人脸图片，然后获取face_id
		count = 0
		for image in image_list:
			count = count + self.addFaceToPerson(image, person_name)
		print count, "张图片添加成功"

	'''
	Function:		addPersonToGroup
	Description:	向组内添加一名用户
	Input:			group_name 组名
					person_name 用户名
	Return: 		无
	'''
	def addPersonToGroup(self, person_name, group_name = "face"):
		self.api.group.add_person(group_name = group_name, person_name = person_name)
		print "添加" + person_name + "到" + group_name

	'''
	Function:		createGroup
	Description:	向人脸库添加一个组
	Input:			group_name 组名
					person_name_list 用户名列表
	Return: 		无
	'''
	def createGroup(self, group_name = "face", person_name_list = []):
		try:
			self.api.group.delete(group_name = group_name)
		except:
			pass
		self.api.group.create(group_name = group_name)
		for person_name in person_name_list:
			self.addPersonToGroup(group_name = group_name, person_name = person_name)

	'''
	Function:		train
	Description:	在组内进行训练，以便得到人脸特征库
	Input:			group_name 组名
	Return: 		无
	'''
	def train(self, group_name = "face"):
		print "训练开始"
		session = self.api.train.identify(group_name = group_name)
		if session['session_id'] == None:
			return
		session = self.api.info.get_session(session_id = session['session_id'])
		while(None != session ):
			time.sleep(1)
			if session['status'] != None and session['status'] != 'INQUEUE':
				break
			session = self.api.info.get_session(session_id = session['session_id'])
		print "训练结束"
		return

	'''
	Function:		identify
	Description:	在组内识别某张图片是否属于组内某个人，是的话返回该人名
	Input:			imageUrl 图片链接（需要提供包括ip地址的完整链接）
					group_name 组名
	Return: 		0 无法定位到脸
					1 该脸部在人脸库中找到
					-1 该脸部不在人脸库中
	'''
	def identify(self, imageUrl, group_name = "face"):
		#f = open(imageUrl, 'rb')
		#bin_data = f.read()
		#f.close()
		print '识别开始'
		#print '人脸库数据：'
		#print self.api.group.get_info(group_name = group_name)
		#print 
		result = self.api.recognition.identify(url = imageUrl, group_name = group_name)

		#排除找不到脸或者找到多张脸的情况
		if len(result['face']) !=  1:
			print 'no face or more than one face found!'
			return 0

		print result
		face = result['face'][0]
		if len(face['candidate']) <  1:
			print 'This person is NOT in our database!'
			return -1
			
		maxCandidate = None
		for candidate in face['candidate']:
			print candidate['confidence'], candidate['person_name']
			if maxCandidate == None or maxCandidate['confidence'] < candidate['confidence']:
				maxCandidate = candidate			
	
		if maxCandidate['confidence'] > CONFIDENCE_LIMIT:
			print maxCandidate['person_name'], 'is found with ', imageUrl
			return maxCandidate['person_name']
		else:
			print 'This person is NOT in our database!', imageUrl
			return -1

#fd = FaceDetector()
#fd.createGroup()
#fd.delPerson('troy')

#!/usr/bin/python
# -*- coding:utf-8 -*-

from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.utils import simplejson

import pycurl

import time
import datetime
import json

from faceDetector import FaceDetector
from sql import MysqlDB

from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, RequestContext

import threading

import StringIO, cStringIO, sys

import sys  
  
reload(sys)  
sys.setdefaultencoding('utf8') 

from django import forms
class UploadFileForm(forms.Form):
	person_name = forms.CharField(max_length=50)
	file = forms.FileField()

class UploadFileForm2(forms.Form):
        file = forms.FileField()

def hello(request):
        a = ""
	if request.method == 'POST':
		a = str(request.POST['what'])
	return HttpResponse("Hello world" + a)

def getPersonList(request):
	db = MysqlDB()
	data = db.selectAll()
	db.close()
	return HttpResponse(simplejson.dumps(data, ensure_ascii=False))

@csrf_exempt
def postAddPerson(request):
	data = {}
	if request.method == 'POST':
		person_name = str(request.POST['person_name'])
		print person_name
		t = AddPersonThread(person_name)
		t.start()
		data = {"person_name": person_name}
	render_to_response('postAddPerson.html',context_instance=RequestContext(request))
	return HttpResponse(simplejson.dumps(data, ensure_ascii=False))

class AddPersonThread(threading.Thread):
        def __init__(self, person_name):
                threading.Thread.__init__(self)
                self.person_name = person_name
        def run(self):
		time1 = time.time()
		fd = FaceDetector()
		fd.addPerson(person_name = self.person_name)
		fd.addPersonToGroup(person_name = self.person_name)
	        time2 = time.time()
		print time2 - time1
@csrf_exempt
def postAddFace(request):
	data = {}
	if request.method == 'POST':
		form = UploadFileForm(request.POST, request.FILES)
		#print form
		if form.is_valid():
        		#handle_uploaded_file(request.FILES['file'])
			file_obj = request.FILES['file']
			print file_obj
			content = file_obj.read()
                	f = StringIO.StringIO(content)
                	
			from PIL import Image 
			image = Image.open(f)
                	image = image.convert('RGB')
                	person_name = str(request.POST['person_name'])
                	import os
			abs_name = os.path.dirname(__file__) + '/albums/%s_%s' %(time.strftime('%Y-%m-%d',time.localtime(time.time())),str(file_obj))
                	image.save(file(abs_name, 'wb'))
                	t = AddFaceThread(abs_name, person_name)
                	t.start()
			data = {"person_name": person_name, "file": abs_name}
        else:
                form = UploadFileForm()
        render_to_response('postAddFace.html', {'form': form})
        return HttpResponse(simplejson.dumps(data, ensure_ascii=False))

class AddFaceThread(threading.Thread):
	def __init__(self, image, person_name):
		threading.Thread.__init__(self)
		self.image = image
		self.person_name = person_name
	def run(self):
		time1 = time.time()
		fd = FaceDetector()
		fd.addFaceToPerson(self.image, self.person_name)
		time2 = time.time()
		print time2 - time1

@csrf_exempt
def postTrain(request):
	data = {"request":"train"}
	t = TrainThread()
	t.start()
	return HttpResponse(simplejson.dumps(data, ensure_ascii=False))

class TrainThread(threading.Thread):
        def __init__(self):
                threading.Thread.__init__(self)
        def run(self):
		time1 = time.time()
                fd = FaceDetector()
                fd.train()
		time2 = time.time()
		print time2 - time1

@csrf_exempt
def postFaceToIdentify(request):
        time1 = time.time()

	data = {}
        if request.method == 'POST':
		#print request
                form = UploadFileForm2(request.POST, request.FILES)
                #print form
                if form.is_valid():
                        #print "asdfsad"
			#handle_uploaded_file(request.FILES['file'])
                        file_obj = request.FILES['file']
                        #print file_obj
                        content = file_obj.read()
                        f = StringIO.StringIO(content)

                        from PIL import Image
                        image = Image.open(f)
                        image = image.convert('RGB')
                        import os
			image_name = '/tmp/%s_%s' %(time.strftime('%Y-%m-%d',time.localtime(time.time())),str(file_obj))
                        abs_name = os.path.dirname(__file__) + image_name
                        image.save(file(abs_name, 'wb'))
                        imageUrl = "http://115.28.246.138:8080" + image_name
			print imageUrl
        		fd = FaceDetector()
        		ret = fd.identify(imageUrl)
        
        		if ret == 0:
        		        data = {"error" : "no face or more than one face found!"}
        		elif ret == -1:
                		data = {"error" : "This person is NOT in our database!"}
        		else:
				print "ret:", ret
                		person_name = str(ret)
                		db = MysqlDB()
                		data = db.select(person_name)
        else:
                form = UploadFileForm()
        
	time2 = time.time()
        print time2 - time1

	render_to_response('postFaceToIdentify.html', {'form': form})
        return HttpResponse(simplejson.dumps(data, ensure_ascii=False))


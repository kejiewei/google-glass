from django.conf.urls import patterns, include, url

from django.contrib import admin
from server.views import *
admin.autodiscover()

from django.conf import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'server.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    ('^hello/$', hello),
    ('^getPersonList/$',getPersonList),
    ('^postAddPerson/$',postAddPerson),
    ('^postTrain/$',postTrain),
    ('^postAddFace/$',postAddFace),
    ('^postFaceToIdentify/$',postFaceToIdentify),
    (r'^tmp/(?P<path>.*)$','django.views.static.serve', {'document_root':settings.STATICFILES_DIRS,'show_indexes': True}),
    url(r'^admin/', include(admin.site.urls)),
)

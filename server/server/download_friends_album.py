# -*-coding:utf-8-*-
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

import sys
import os
import re

import urllib
import urllib2
import cookielib

from bs4 import BeautifulSoup

USERNAME = "907925145@qq.com"
PASSWORD = "kjw19910513"
USERID = "302202827"

from sql import *

class RRAD():
	def __init__(self):
		# initialize the download dir.
		self.download_dir = 'albums'
		if not os.path.isdir(self.download_dir):
			os.mkdir(self.download_dir)

		# build the session
		self.session = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookielib.CookieJar()))
		
	# sign into renren.com
	def signin(self):
		#username = raw_input('username: ')
		#password = getpass.getpass('password: ')
		username = USERNAME
		password = PASSWORD
		data = (
			('email', username),
			('password',password),
			('origURL',"http://www.renren.com/Home.do"),
			('domain',"renren.com"))
		page = self.session.open('http://www.renren.com/PLogin.do', urllib.urlencode(data))
		page.close()

	def signout(self):
		self.session.open('http://www.renren.com/Logout.do')

	# fetch source code by post
	def do_post(self, url):
		return self.session.open(url, {}).read()

	# fetch source code by get
	def do_get(self, url):
		return self.session.open(url).read()

	# get the album's photo links
	def get_album_info(self, album_url):
		photo_links = [] # photo links in all pages
		album_name = album_url.split('/')[-1]
		# fetching links page by page
		content = self.do_get(album_url)
		links = re.findall(r'<a.*href="(.*)" class="picture">', content)
		if links:
			photo_links.extend([re.sub(r'\?.*$', '', link) for link in links])
		return { 'album_name': album_name, 'photos': photo_links }
	
	def get_photo_file(self, photo_url):
		content = self.do_get(photo_url + '/large?xtype=album')
		open('c.txt', 'w').write(content)
		match = re.search(r'<div id="large-con"(.*?)src="(?P<src>.*?)" class="photo"', content, 
				flags=re.MULTILINE|re.DOTALL|re.IGNORECASE)
		return match and match.group('src')

	# download the photo into the given album directory 
	def save_photo_file(self, album_dir, photo_file):
		try:
			print photo_file
			filename = photo_file.split('/')[-1]
			f = open(os.path.join(album_dir, filename), 'wb')
			f.write(self.session.open(photo_file).read())
			f.close()
			return True
		except Exception, e:
			return False
	
	# download the album
	def save_album(self, url, username):
		album_url = re.sub(r'[\?\#].*$', '', url)
		
		album_info = self.get_album_info(album_url)

		# create the album directory if not exists
		album_dir = os.path.join(self.download_dir, username)
		if not os.path.isdir(album_dir): os.mkdir(album_dir)
		
		# download each photo into the album directory
		print 'saving album to', album_dir
		for i, link in enumerate(album_info['photos']):
			print '(%d/%d) %s' % (i + 1, len(album_info['photos']), link), 
			try:
				photo_file = self.get_photo_file(link)
				self.save_photo_file(album_dir, photo_file)
				print 'saved.' 
			except:
				print 'failed.'
		print 'all downloads completed.'
		
	def get_friend_list(self):
		'''friendUrl = 'http://friend.renren.com/myfriendlistx.do'  
		rawHtml = self.do_get(friendUrl)
		print rawHtml
		
		friendInfoPack = re.search(r'var friends=\[(.*?)\];', rawHtml).group(1)  
		friendIdPattern = re.compile(r'"id":(\d+).*?"name":"(.*?)"')  
		friendIdList = []  
		for id, name in friendIdPattern.findall(friendInfoPack):  
			friendIdList.append((id, Str2Uni(name)))  
		
		return friendIdList  '''
		
		pagenum = 0
		friendIdList = []  
		
		print u"开始解析好友列表"

		while True:
			friendUrl = "http://friend.renren.com/GetFriendList.do?curpage=" + str(pagenum) + "&id=" + str(USERID)
			html = self.do_get(friendUrl)
			#print html
			pattern = '<a href="http://www\.renren\.com/profile\.do\?id=(\d+)"><img src="http://[\S]*" alt="[\S]*[\s]\((.*)\)" />'
			m = re.findall(pattern, html)#查找目标
			if len(m) == 0:
				break#不存在
			for id, name in m:  
				friendIdList.append((id, unicode(str(name), "utf8")))
			pagenum += 1
		
		print u"好友列表分析完毕."
		return friendIdList
    
	def get_friend_info(self, friend_id):
		infoDict = {}
		fieldDict = {"性别": "sex", "生日": "birth_date", "家乡": "hometown", "手机" : "mobile", "QQ" : "qq", "大学" : "college", "高中" : "s_high_school", "初中" : "j_high_school", "小学" : "primary_school"}
		url = "http://www.renren.com/" + str(friend_id) + "/profile?v=info_timeline"
		#print url
		html = self.do_get(url)
		#print html
		pattern = '<title>人人网 - (.*)'
		name = re.findall(pattern, html)[0].decode("utf8")
		print name
		infoDict["name"] = "asdf"
		infoDict["renren"] = friend_id

		soup = BeautifulSoup(html)
		for dl in soup.findAll('dl', {'class' : 'info'}):
			#print dl
			tmpStr = ""
			print dl.dt.string
			for child in dl.dd.children:
				if child.string:
					tmpStr = tmpStr + child.string.strip("\r\n")
				else:
					tmpStr = tmpStr + "\r\n"
			print tmpStr
			infoDict[fieldDict.get(eval('"'+dl.dt.string+'"'))] = tmpStr
		
		return infoDict
		
if __name__ == '__main__':
	rrad = RRAD()
	rrad.signin()
	
	
	#获取好友列表，下载好友头像相册
	'''friendList = rrad.get_friend_list()
	for id, name in friendList:
		soup = BeautifulSoup(rrad.do_get("http://photo.renren.com/photo/" + str(id) + "/album/relatives"))
		p = soup.findAll('a', {'class' : 'album-title'})
		for a in p:
			if unicode(str(a), "utf8").find(u"头像相册") != -1:
				#print str(a).decode('utf8')
				#print a['href']
				rrad.save_album(a['href'], name)'''
	
	infoDict = rrad.get_friend_info("279336569")
	print infoDict
	db = MysqlDB()
	db.insert(infoDict)
	db.selectAll()
	db.close()
		
